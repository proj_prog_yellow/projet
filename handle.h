#ifndef DEF_HANDLE_H
#define DEF_HANDLE_H

#include "game.h"
#include "objects.h"

#define HEIGHT 450
#define WIDTH 800

void handle_event_start_screen (SDL_Event event);
void handle_event (SDL_Event event, s_objects star, s_objects shot);
void handle_key (SDL_Event event, s_objects star);

#endif
