#ifndef DEF_TEXTE_H
#define DEF_TEXTE_H

#include "game.h"

void scores (int *tab, int size);
void recup_scores (int *tab, int size);
void insert_val (int *tab, int size, int val);

#endif
