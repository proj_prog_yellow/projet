#define HEIGHT 450
#define WIDTH 800

#include "objects.h"

double angle;
unsigned int gameover, wait_shot;
double a,b,c;


s_objects init_sprite_star (){
  s_objects sprite;

  sprite  = (s_objects)malloc(sizeof(struct Objects));

  FILE* file = NULL;
  file = fopen("sprite_principal.txt", "r");
  
  int tab[4];
  int i = 0;
  for (i=0; i<4; i++) {
    fscanf(file, "%d", &tab[i]);
    printf ("%d" , tab[i]);
  }
  
  fclose(file);
  
  sprite->speed = tab[0];
  sprite->s_size = tab[1];
  
  sprite->pic_pos.x = 0;
  sprite->pic_pos.y = 0;
  sprite->pic_pos.w = sprite->s_size;
  sprite->pic_pos.h = sprite->s_size;
  
  sprite->pos.x = tab[2];
  sprite->pos.y = tab[3];

  return sprite;
}

s_objects init_sprite_shot (s_objects star){
  s_objects sprite;

  sprite  = (s_objects)malloc(sizeof(struct Objects));

  sprite->state = 0;
  sprite->speed = 5;
  sprite->s_size = 29;

  sprite->pic_pos.x = star->pos.x;
  sprite->pic_pos.y = star->pos.y;
  sprite->pic_pos.w = sprite->s_size;
  sprite->pic_pos.h = sprite->s_size;

  sprite->pos.x = star->pos.x;
  sprite->pos.y = star->pos.y;

  return sprite;
}

void handle_shot (s_objects shot, s_objects star) {
  if (wait_shot) {
    if (out_of_bounds(shot)) {
      wait_shot = 0;
      shot->pos.x = 0;
      shot->pos.y = 0;
    }
    else {
      shot->pos.x += shot->speed;
      shot->pos.y = (-(a*shot->pos.x + c)/b);
    }
  }
}

s_objects init_sprite_obstacle (int y_pos) {

  s_objects sprite;
  sprite = (s_objects)malloc(sizeof(struct Objects));

  sprite->s_size = 100;
  sprite->state = 1;

  sprite->pic_pos.x = 0;
  sprite->pic_pos.y = 0;
  sprite->pic_pos.w = sprite->s_size;
  sprite->pic_pos.h = sprite->s_size;

  sprite->pos.x = WIDTH-50;
  sprite->pos.y = y_pos;

  return sprite;
}

/*
s_list new_obstacle (s_list L) {
	
  int rand_pos = 4 + rand()%3;
  int up = 0, middle = 150, down = 300;	 
	
  switch (rand_pos) {
  case 1:
    L = cons(init_sprite_obstacle(up),L);
    break;
  case 2:
    L = cons(init_sprite_obstacle(middle),L);
    break;
  case 3:
    L = cons(init_sprite_obstacle(down),L);
    break;
  case 4:
    L = cons(init_sprite_obstacle(up), 
	     cons(init_sprite_obstacle(middle),L));
    break;
  case 5:
    L = cons(init_sprite_obstacle(up),
	     cons(init_sprite_obstacle(down),L));
    break;
  case 6:
    L = cons(init_sprite_obstacle(middle), 
	     cons(init_sprite_obstacle(down),L));
    break;
  case 7:
    L = cons(init_sprite_obstacle(middle), 
	     cons(init_sprite_obstacle(down),
		  cons(init_sprite_obstacle(up),L)));
    break;
  }
  return L;
}
*/

s_list new_obstacle (s_list L) {

  int rand_nb_w = 1 + rand()%2;
  int rand_pos1 = -70 + rand()%440;

  if (rand_nb_w == 1) {
    L = cons(init_sprite_obstacle(rand_pos1),L);
  } else {
    if (rand_pos1 > HEIGHT / 2) {
      int rand_pos2 = (rand_pos1 - 160 ) + rand()%(rand_pos1 - 80);
      L = cons(init_sprite_obstacle(rand_pos1), cons(init_sprite_obstacle(rand_pos2),L));
    } else {
      int rand_pos2 = (rand_pos1 + 160 ) + rand()%(rand_pos1 + 80);
      L = cons(init_sprite_obstacle(rand_pos1), cons(init_sprite_obstacle(rand_pos2),L));
    }
  }
  return L;
}

s_list handle_obstacles (s_objects shot, s_list obstacles) {
 
  s_list new = empty_list();
  while (!is_empty(obstacles)) {
      
    if (prem(obstacles)->pos.x <= 0) {
      prem(obstacles)->state = 0;
    }
    if (collision_objects (shot, prem(obstacles))) {
      wait_shot = 0;
      shot->pos.x = 0;
      shot->pos.y = 0;
      prem(obstacles)->state = 0;
    }	
    prem(obstacles)->pos.x -= GLOBAL_SPEED;
    if (prem(obstacles)->state != 0) {
      new = cons(prem(obstacles),new);
    }
    obstacles = reste(obstacles);      
  }
  return new;
}

bool collision_objects (s_objects a, s_objects b)
{
  bool collision = true;

  if (a->pos.x > b->pos.x + b->s_size) {
    collision = false;
  }
  if (a->pos.y + a->s_size < b->pos.y) {
    collision = false;
  }
  if (a->pos.x + a->s_size < b->pos.x) {
    collision = false;
  }
  if (a->pos.y > b->pos.y + b->s_size) {
    collision = false;
  }
  
  return collision;
}  

bool out_of_bounds (s_objects object) {
  if (object->pos.x > WIDTH || object->pos.x < 0 || 
      object->pos.y > HEIGHT || object->pos.y < 0) {
    return true;
  }
  return false;
}
