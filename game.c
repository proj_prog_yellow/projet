#include "handle.h"

#include "game.h"
#include "objects.h"
#include "lists.h"
#include "texte.h"

SDL_Rect pos_mouse;
unsigned int gameover, wait_shot, cpt_obstacles, before_game;


int main (int argc, char** argv)
{
  srand(time(NULL));

  // Variables
  before_game = 1;
  gameover = 1;
  wait_shot = 0;
  cpt_obstacles = 0;
  s_list Obstacles = empty_list();
  s_objects star = init_sprite_star();
  s_objects shot = init_sprite_shot(star);
  Obstacles = new_obstacle(Obstacles);
  int tab_scores[5] = {0};

  int size = 5;
  int score_game = 0;

  // Open window
  SDL_Surface* screen = SDL_SetVideoMode(WIDTH, HEIGHT, 16, SDL_HWSURFACE|SDL_DOUBLEBUF);

  SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));


  // Load background
  SDL_Surface* background = SDL_LoadBMP("background.bmp");

  Uint32 background_x = 0;

  SDL_Rect back_gr;

  back_gr.x = 0;
  back_gr.y = 0;
  back_gr.w = 5000;
  back_gr.h = HEIGHT;

  SDL_Rect back_gr_start;

  back_gr_start.x = 0;
  back_gr_start.y = 0;
  back_gr_start.w = WIDTH;
  back_gr_start.h = HEIGHT;

  SDL_Surface* title = SDL_LoadBMP("title.bmp");

  // Load sprites
  SDL_Surface* sprite_star = SDL_LoadBMP("star.bmp");
  SDL_Surface* sprite_shot = SDL_LoadBMP("shot.bmp");
  SDL_Surface* sprite_obstacles = SDL_LoadBMP("crazy_watermelon.bmp");

  int colorkey = SDL_MapRGB(sprite_star->format, 88, 213, 0);
  int colorkey_shot = SDL_MapRGB(sprite_shot->format, 0, 0, 0);

  SDL_SetColorKey(sprite_star, SDL_SRCCOLORKEY | SDL_RLEACCEL, colorkey);
  SDL_SetColorKey(sprite_obstacles, SDL_SRCCOLORKEY | SDL_RLEACCEL, colorkey);
  SDL_SetColorKey(sprite_shot, SDL_SRCCOLORKEY | SDL_RLEACCEL, colorkey_shot);
    
  SDL_Event event;


  SDL_BlitSurface(title, 0, screen, &back_gr);
  SDL_Flip(screen);
  SDL_Delay(2000);

  // Start game loop
  while (gameover){

    // Handle events
    if (SDL_PollEvent(&event)) {
      handle_event(event, star, shot);
    }

    handle_key (event,star);

    // Handle background
    back_gr.x = background_x;
    background_x -= GLOBAL_SPEED;
    if (back_gr.x <= -back_gr.w-WIDTH){
      back_gr.x = 0;
      background_x = 0;
    }

    score_game ++;

    // Handle objects
    handle_shot (shot, star);

    // New obstacles
    if (cpt_obstacles > 10000) {
      cpt_obstacles = 0;
    }
    if (cpt_obstacles % 100 == 0) {
      Obstacles = new_obstacle(Obstacles);
    }

    Obstacles = handle_obstacles (shot, Obstacles);
	
    cpt_obstacles += 3;
	
    // Clear screen
    SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));

    // Blit sprites
    SDL_BlitSurface(background, NULL, screen, &back_gr);
    SDL_BlitSurface(sprite_star, 0, screen, &star->pos);
	
    s_list test = Obstacles;
    while (!is_empty(test)){
      if (collision_objects (star, prem(test))) {
	gameover = 0;
	SDL_Delay(1000);
      }
      SDL_BlitSurface(sprite_obstacles, 0, screen, &prem(test)->pos);
      test = reste(test);
    }

    if (wait_shot) {
      SDL_BlitSurface(sprite_shot, 0, screen, &shot->pos);
    }

    SDL_Flip(screen);
  }

  recup_scores (tab_scores, size);
  insert_val (tab_scores, size, score_game);
  scores (tab_scores, size);
  SDL_FreeSurface(background);
    

  free_list(Obstacles);
  free(sprite_shot);
  free(sprite_star);
  free(sprite_obstacles);
  return 0;
}
