#ifndef DEF_LISTS_H
#define DEF_LISTS_H

#include "game.h"
#include "objects.h"


struct List {
  s_objects prem;
  s_list reste;
};


s_list empty_list ();
s_list cons (s_objects w, s_list L);
bool is_empty (s_list L);
s_objects prem (s_list L);
s_list reste (s_list L);
void free_list (s_list L);

#endif
