#include "handle.h"

double angle;
unsigned int gameover, wait_shot, before_game;
SDL_Rect pos_mouse;
double a,b,c;

void handle_event_start_screen (SDL_Event event) {
  while (before_game) {

    switch (event.type) {

    case SDL_QUIT:
      before_game = 0;
      break;

    case SDL_KEYDOWN:
      switch (event.key.keysym.sym) {
      case SDLK_SPACE:
	before_game = 0;
	break;
      }
      break;

    }
  }
}

void handle_event(SDL_Event event, s_objects star, s_objects shot) {
  switch (event.type) {
  case SDL_QUIT:
    gameover = 0;
    break;
  case SDL_MOUSEBUTTONDOWN:
    if (!wait_shot) {
      pos_mouse.x = event.button.x;
      pos_mouse.y = event.button.y;

      shot->pos = star->pos;

      a =  (pos_mouse.y - shot->pos.y);
      b = -(pos_mouse.x - shot->pos.x);
      c = -(a*shot->pos.x) - (b*shot->pos.y);
    }
    wait_shot = 1;
    break;
  }
}

void handle_key (SDL_Event event, s_objects star){
  Uint8 *keystate = SDL_GetKeyState(NULL);
  if (keystate[SDLK_ESCAPE]){
    gameover = 0;
  }
  if (keystate[SDLK_d]) {
    if (star->pos.y < HEIGHT - star->s_size) {
      star->pos.y += star->speed;
    }
  }
  if (keystate[SDLK_z]) {
    if (star->pos.y > 0) {
      star->pos.y -= star->speed;
    }
  }
}
