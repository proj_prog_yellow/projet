#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include <SDL/SDL.h>

#define HEIGHT 450
#define WIDTH 800
#define GLOBAL_SPEED 4

typedef struct Objects* s_objects;
typedef struct List* s_list;

int main (int argc, char** argv);
