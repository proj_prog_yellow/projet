#ifndef DEF_OBJECTS_H
#define DEF_OBJECTS_H

#include "game.h"
#include "lists.h"

struct Objects {
  unsigned int state;
  unsigned int speed;
  unsigned int s_size;
  SDL_Rect pic_pos;
  SDL_Rect pos;
};

s_objects init_sprite_star ();
s_objects init_sprite_shot (s_objects star);
s_objects init_sprite_obstacle (int y_pos);

void handle_shot (s_objects shot, s_objects star);
s_list new_obstacle (s_list L);
s_list handle_obstacles (s_objects shot, s_list obstacles);

bool collision_objects (s_objects a, s_objects b);
bool out_of_bounds (s_objects object);


#endif
