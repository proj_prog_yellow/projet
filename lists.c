#include "lists.h"

s_list empty_list () {
  return NULL ;
}

s_list cons (s_objects w, s_list L) {
  s_list M;
  M = malloc(sizeof(*M));
  M->prem = w;
  M->reste = L;
  return M;
}

bool is_empty (s_list L) {
  return L == NULL;
}

s_objects prem (s_list L) {
  if (!is_empty(L)) {
    return L->prem;
  }
}

s_list reste (s_list L) {
  return L->reste;
}

void free_list (s_list L) {
  if (is_empty(L)) {
    return;
  }
  free_list(reste(L));
  free(L);
}
